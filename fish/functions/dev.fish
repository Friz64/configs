# Defined in - @ line 2
function dev
	for arg in $argv
        cd ~/Documents/rust/$arg
    end
    tmux new-session -s "dev" -d 'nvim; tmux kill-session'
    tmux split-window -v
    tmux -2 attach-session -d \; resize-pane -y 11 \; last-pane
end
