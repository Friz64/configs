let mapleader = ' '
set hidden
set autoread
set number relativenumber
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set scrolloff=3
set mouse=a
filetype plugin on
syntax on
set termguicolors

call plug#begin(stdpath('data') . '/plugged')
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'lotabout/skim.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'morhetz/gruvbox'
Plug 'moll/vim-bbye'
Plug 'airblade/vim-rooter'
Plug 'aurieh/discord.nvim', { 'do': ':UpdateRemotePlugins'}
Plug 'cespare/vim-toml'
Plug 'tikhomirov/vim-glsl'
Plug 'jiangmiao/auto-pairs'
Plug 'rust-lang/rust.vim'
call plug#end()

" Clear search highlight
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>

" COLEMAK: Swap: Left 
noremap <silent> t h
noremap <silent> T H
noremap <silent> h t
noremap <silent> H T
"noremap <silent> <C-T> b

" COLEMAK: Swap: Right 
noremap <silent> n l
noremap <silent> N L
noremap <silent> l n
noremap <silent> L N
"nnoremap <silent> <C-N> w

" COLEMAK: Swap: Up 
noremap <silent> m k
noremap <silent> M K
noremap <silent> k m
noremap <silent> K M

" COLEMAK: Swap: Down 
noremap <silent> c j
noremap <silent> C J
noremap <silent> j c
noremap <silent> J C

" Buffer management
" COLEMAK: Mapped to movement keys
map <silent> <leader>o :Files<CR>
map <silent> <leader>s :Buffers<CR>
map <silent> <leader>t :bp<CR>
map <silent> <leader>n :bn<CR>
map <silent> <leader>r :Bdelete<CR>
map <silent> <leader><leader> <C-^>

" skim configuration
let $SKIM_DEFAULT_COMMAND = "rg --files"

" airline configuration
let g:airline_powerline_fonts = 1
let g:airline_theme='gruvbox'
let g:airline#extensions#tabline#enabled = 1

" Color configuration
set background=dark
colorscheme gruvbox

" Language Client stuff
au User lsp_setup call lsp#register_server({
        \ 'name': 'rust-analyzer',
        \ 'cmd': {sever_info->['rust-analyzer']},
        \ 'whitelist': ['rust'],
        \ })
function! s:on_lsp_buffer_enabled() abort
    setlocal signcolumn=yes
    "autocmd BufWritePre *.rs LspDocumentFormatSync
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gh <plug>(lsp-hover)
    nmap <buffer> gf <plug>(lsp-code-action)
    nmap <buffer> <f2> <plug>(lsp-rename)
endfunction
augroup lsp_install
    au!
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END
let g:lsp_highlight_references_enabled = 1
highlight link LspErrorText GruvboxRedSign
highlight link LspWarningText GruvboxYellowSign
highlight LspReference guibg=#504945
highlight link LspErrorHighlight GruvboxRedBold
highlight link LspWarningHighlight GruvboxYellowBold
highlight clear LspErrorLine
highlight clear LspWarningLine

" Completion
imap <c-space> <Plug>(asyncomplete_force_refresh)
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? "\<C-y>" : "\<cr>"

" Rustfmt
let g:rustfmt_autosave = 1
let g:rustfmt_fail_silently = 1

