# configs

It is important to note that

- i have no idea how vimscript works, i just magically hack everything together.
- i use the [Colemak keyboard layout](https://colemak.com/), therefore i need to remap some keys. I try to stay close to the default vim config though.
